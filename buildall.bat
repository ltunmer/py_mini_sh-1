@echo off
REM ****************************************************************************
REM *
REM * QUALCOMM_COPYRIGHT_STATEMENT
REM *
REM ****************************************************************************

REM Setup this folder as an environment for the buildwarehouse builds.
REM It creates a Python virtualenv and activates it so that pythons
REM scripts are on the path.
REM
REM Whenever you switch to different branches, re-run activate in that folder.
REM
REM To forget the path additions, run:
REM
REM   (pyenv36) C:>  deactivate
REM
REM and this will revert the path to what is was before. The (pyenv36) prefix
REM should disappear from the shell prompt.

set PY_LOC=
set VENV=

IF "%1"=="" (
    ECHO Specify either 27, 34, 35 or 36 as an argument
    GOTO :FAILED
) ELSE IF "%1"=="27" (
    ECHO Building for Python 2.7
    set PYVER=2.7
) ELSE IF "%1"=="34" (
    ECHO Building for Python 3.4
    set PYVER=3.4
) ELSE IF "%1"=="35" (
    ECHO Building for Python 3.5
    set PYVER=3.5
) ELSE IF "%1"=="36" (
    ECHO Building for Python 3.6
    set PYVER=3.6
) ELSE (
    ECHO Specify either 27, 34, 35 or 36 as an argument
    set ERRORLEVEL=1
    GOTO :FAILED
)
set VENV=pyenv%1

if "%PYVER%"=="2.7" (
    REM Install a python 2.7 virtualenv
    set PY_LOC=C:\python27
)
if "%PYVER%"=="2.7" (
    if NOT EXIST %VENV%\NUL (
        %PY_LOC%\Scripts\pip install -U virtualenv
        IF errorlevel 1 GOTO FAILED
        %PY_LOC%\Scripts\virtualenv  %VENV%
        IF errorlevel 1 GOTO FAILED
    )
    %VENV%\Scripts\pip install -e .
    IF errorlevel 1 GOTO FAILED
    GOTO RUNPY
)

REM Let's see if Python3? has been installed elsewhere before.
REM (this obscure syntax does the same thing that bash backticks do!)
for /F "usebackq tokens=*" %%P in (`c:\Windows\py.exe -%PYVER% -c "import sys,os; print(os.path.dirname(sys.executable))"`) do set PY_LOC=%%P

IF "%PY_LOC%"=="" (
    echo Please install the Python version %PYVER%.
    set ERRORLEVEL=1
    GOTO :FAILED
)

REM Build the virtualenv if it doesn't exit
if NOT EXIST %VENV%\NUL (
    %PY_LOC%\python.exe -m venv %VENV%
    REM Install the py_mini_sh package
    %VENV%\Scripts\pip install -e .
    IF errorlevel 1 GOTO FAILED
)

:RUNPY
REM Run the buildall.py to do everything else
%VENV%\Scripts\python.exe buildall.py
IF errorlevel 1 GOTO FAILED
 
call %VENV%\Scripts\activate.bat
GOTO :EOF

:FAILED
ECHO Failed to build
EXIT /B %ERRORLEVEL%
 
