
"""
Bootstrap a python build environment for the py_mini_sh project.
"""
import sys
import logging

from py_mini_sh import exec_, pipe, run, is_f, del_
from py_mini_sh import parse_pylint_output, readall, writeall, BuildError

# Project defines:
PYLINT_OUT = 'py_mini_sh-pylint'
PYTEST_COVER_ARGS = '--cov=py_mini_sh --cov-report=html:SHIP-{ALLVARIANTS}/htmlcov'
PYLINT_OUT_WORSE = '{PYLINT_OUT}-worse'


def bootstrap():
    # Update core stuff in our virtualenv
    exec_('{PYTHON_EXEC} -m pip install -U pip setuptools wheel')
    
    # Install the project in editable mode with its testing requirements.
    exec_('{VENV_BIN}pip install -e ".[testing]"')

    # build the sphinx doc
    exec_('{VENV_BIN}python -m sphinx -b html -d ../build/doctrees source ../SHIP-{ALLVARIANTS}/docs/html',
          cwd="docs")

    # run tests.
    exec_('{VENV_BIN}pytest {PYTEST_COVER_ARGS}')

    # run pylint, and check that its score is not worse than the last committed one
    pylint_data = pipe('{VENV_BIN}pylint py_mini_sh', ignore_error=True)
    e, w, c, r = parse_pylint_output(pylint_data)
    if is_f('{PYLINT_OUT}'):
        E, W, C, R = parse_pylint_output(readall('{PYLINT_OUT}'))
        logging.info("""
            Pylint results:
                Was Error: %d, Warning: %d, Convention: %d, Refactor: %d
                Now Error: %d, Warning: %d, Convention: %d, Refactor: %d
        """,
        E, W, C, R, e, w, c, r)
        if e > E or w > W or c > C or r > R:
            writeall('{PYLINT_OUT_WORSE}', pylint_data)
            raise BuildError("Pylint results worse than before")
    writeall('{PYLINT_OUT}', pylint_data)
    del_('{PYLINT_OUT_WORSE}')

    # if we've got this far, generate the wheels
    exec_('{VENV_BIN}python setup.py bdist_wheel --dist-dir SHIP-{ALLVARIANTS}/wheel')


if __name__ == '__main__':
    sys.exit(run(bootstrap, globals()))
